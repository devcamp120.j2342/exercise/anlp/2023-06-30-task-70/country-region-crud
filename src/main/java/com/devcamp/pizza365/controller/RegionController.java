package com.devcamp.pizza365.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private CountryRepository countryRepository;

	@CrossOrigin
	@PostMapping("/region/create/{country_id}")
	public ResponseEntity<Object> createRegion(@PathVariable("country_id") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CRegion newRegion = new CRegion();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				CCountry _country = countryData.get();
				newRegion.setCountry(_country);
				newRegion.setCountryName(_country.getCountryName());

				CRegion savedRole = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/region/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			CRegion newRegion = regionData.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());
			CRegion savedRegion = regionRepository.save(newRegion);
			return new ResponseEntity<>(savedRegion, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/region/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/region/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@GetMapping("/region/all")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions")
	public List<CRegion> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
	public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		return regionRepository.findByIdAndCountryId(regionId, countryId);
	}

	@CrossOrigin
	@GetMapping("/country/{countrycode}/regions/{code}")
	public Optional<CRegion> getRegionByRegionCAndCountryC(@PathVariable(value = "countryCode") String countryCode,
			@PathVariable(value = "code") String regionCode) {
		return regionRepository.findByRegionCodeAndCountryCountryCode(regionCode, countryCode);
	}

	@CrossOrigin
	@GetMapping("/regions/code/{regionCode}")
	public Optional<CRegion> getRegionByRegionC(@PathVariable(value = "regionCode") String code) {
		return regionRepository.findByRegionCode(code);
	}
	@CrossOrigin
	@GetMapping("/regions/name/{regionName}")
	public Optional<CRegion> getRegionByRegionName(@PathVariable(value = "regionName") String name) {
		return regionRepository.findByRegionName(name);
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/region-count")
	public ResponseEntity<Object> countRegionsByCountryId(@PathVariable Long countryId) {
		Optional<CCountry> countryData = countryRepository.findById(countryId);
		if (countryData.isPresent()) {
			int regionCount = regionRepository.countByCountryId(countryId);
			return ResponseEntity.ok(regionCount);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@GetMapping("/region/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}
}
